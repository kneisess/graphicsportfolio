<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<style>  
    div.padded {  
      padding-top: 0px;  
      padding-right: 100px;  
      padding-bottom: 0.25in;  
      padding-left: 100px;  
    }  
  </style> 
<title>Krystyn Neisess  |  CS 184</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
</head>
<body>
<br />
<h1 align="middle">PathTracer</h1>
    <h2 align="middle">Krystyn Neisess</h2>

    <div class="padded">
        <p>In "PathTracer," we implement a variety of key functionalities which comprise a path-tracing renderer.  Beginning with mere ray-intersection with primitives (triangles and spheres), we next explore optimizations such as a BVH (bounding volume hierarchy) acceleration structure, which allow us to complete normal-shading renders of far more complex scenes in a matter of seconds.  Finally, to really bring our rendered images to life, we implement global illumination (that is, direct and indirect lighting) as well as a handful of material BSDFs.<p>

        <p>By it's completion, we are able to render scenes such as the following:<p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part0.png" width="480px" />
                    <figcaption align="middle">Mirror (left) and glass (right) balls within a Cornell Box.  Global illumination.<br>(CBspheres.dae) </figcaption>
                </tr>
            </table>
        </div>

    <h2 align="middle">Part 1:  Ray Generation and Scene Intersection</h2>

        <h3>Overview--Graphics Pipeline</h3>

        <p>This portion of the graphics pipeline is as follows:<p>

        <p> For each pixel to be rendered, camera rays which intersect said pixel are sampled n times.  Each sampled ray is then evaluated for it's radiance.  As this is dependent on what item, if any, in the scene the ray intersects with, one must also check for intersection with the scene's primitives.  The evaluated radiances for each of these samples is then averaged together, as an approximation of the total irradiance over this pixel.<p>

        <p>More procedurally, this can be described as:<p>

        <ol>
            <li>PathTraceter::raytrace_pixel() calls Camera::generate_ray() n times (where n is the number of rays to sample per pixel).</li>

            <li>Each ray is evaluated by PathTracer::trace_ray(), which returns the RGB-spectrum of the radiance corresponding to that ray.</li>

                <ul>
                    <li>PathTracer::trace_ray() tests for intersections with primitives by calling Triangle::intersect() or Sphere::intersect() for triangles and spheres, respectively.</li>

                    <li>On successful intersections, the following information is recorded and used for radiance evaluation:</li>

                        <ul>
                            <li>The t-value of the intersection (for a ray defined as r(t) = o + td, where o is the ray's origin and d the ray's direction as a unit vector).</li>
                            <li>A pointer to the intersected primitive.</li>
                            <li>The surface normal at the point of intersection.</li>
                            <li>The material BSDF of the intersected primitive.</li>
                        </ul>

                </ul>

            <li>Once evaluated, each RGB-spectrum is then averaged and returned.</li>
        </ol>

        <h3>Triangle Intersection Implementation</h3>
 
        <p> Of especial note is the intersection algorithm used to implement triangle intersection:  the Moller-Trumbore algorithm. Conceptually, this algorithm relies on the unique properties of triangle parameterization via barycentric coordinates.  In particular, on is able to define a new coordinate space such that a triangle's points are in terms of it's barycentric coordinates.  In fact, due to the requirement that barycentric coordinates sum to one, our triangle in this new coordinate space is a unit triangle.  Together, this parameterization allows us to define a system of equations like so:</p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/eqn/part1_mt1.png" />
                </tr>
            </table>
        </div>
            
        <p>where O is the origin of the ray; D is the direction of the ray as a unit vector; P0, P1, and P2 are the vertices of the triangle; and their coefficients are their corresponding barycentric coordinates.  Rearranging terms, we end up with the following formulation: </p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/eqn/part1_mt2.png" />
                </tr>
            </table>
        </div>

        <p>to which we can apply Cramer's rule, resulting in the final solution:</p>
        
        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/eqn/part1_mt3.png" />
                </tr>
            </table>
        </div>

        <p>where</p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/eqn/part1_mt4.png" />
                </tr>
            </table>
        </div>

        <p>With our barycentric coordinates b<sub>1</sub> and b<sub>2</sub> computed, due to the previously mentioned transformation to a unit triangle, our intersection test is reduced down to whether b<sub>1</sub> and b<sub>2</sub> sum to a value less than or equal to one.  Lastly, we obtain the surface normal at the point of intersection by performing a linear interpolation of vertex normals weighted by their corresponding barycentric coordinates.</p>

        <h3>Render Showcase</h3>

        <p>After fleshing out this portion of the graphics pipeline, we are able to render small .dae files with normal shading, like so:  <p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part1_spheres_lambertian.png" width="480px" />
                    <figcaption align="middle">Spheres in a Cornell Box.  Normal shading.<br>14 primitives.  (CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part1_gems.png" width="480px" />
                    <figcaption align="middle">Faceted gems in a Cornell Box.  Normal shading.<br>252 primitives.  (CBgems.dae) </figcaption>
                </tr>
            </table>
        </div>

    <h2 align="middle">Part 2:  Bounding Volume Hierarchy</h2>
        
        <p> Implementing an acceleration structure was the next logical step, as without one, we leave ourselves limited to relatively simple scenes.  That is, the computation time for those containing even a few thousand triangles is borderline-abysmal.  As per the section title, our acceleration structure of choice is a bounding volume hierarchy, which offers substantial gains for a relatively simple algorithm.</p>

        <h3>BVH Construction</h3>

        <p>Starting with a root "node" which contains all primitives in the scene, we recursively partition the partitions into two sets that become our node's left and right children.  This partitioning continues until we achieve leaf nodes which contain no more than the maximum allowable quantity of primitives per leaf (as dictated by a passed in parameter).</p>

        <p>To elaborate on the partitioning schema, we first determine the axis along which the current node's bounding box is largest.  We then select the bounding box's centroid's component along this axis as our split point, allocating primitives to the left or right child node depending on whether said primitive's own centroid's component is less than the split point.</p>

        <p>However, in the event that all primitives lie on the same side of the chosen split point, we implement a contingency plan in which an arbitrary primitive is selected as the new split point, and partition accordingly.  (In this heuristic, it has been arbitrarily decided to use the centroid's x-component as the split point.)  While perhaps somewhat naive, it aptly handles this edge case whilst maintaining high performance.</p>

        <h3>BVH Intersection</h3>
        
        <p>When testing for intersection, we first perform preliminary checks to determine whether a ray intersects with a node's bounding box, returning early if there is no intersection.  If there is, we recursively check for intersection with it's children.  Only upon reaching a leaf node do we then check for intersection with each of its individual primitives.</p>

        <p>Of note is a nifty optimization made in checking for individual bounding box intersection.  Ordinarily, ray intersection with planes is computed like so:</p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/eqn/part3_intersect1.png" />
                </tr>
            </table>
        </div>

        <p>where p' is a point on the plane, N is plane's surface normal, o is the ray's origin, and d is the ray's direction vector.  However, given that our bounding boxes are axis aligned, we can reduce computation to the following:</p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/eqn/part3_intersect2.png" />
                </tr>
            </table>
        </div>

        <p>From there, we simply observe the intersection of the intervals of t-values per axis.  If this yields a non-empty interval, then our ray intersects with the bounding box.</p>

        <h3>Render Showcase</h3>

        <p>With this acceleration structure in place, complex renders such as the ones below require at most a couple seconds!</p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part2_bunny.png" width="480px" />
                    <figcaption align="middle">A bunny sitting on a stone.  Normal shading.<br>33,696 primitives, 0.2800 seconds. (bunny.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part2_CBdragon.png" width="480px" />
                    <figcaption align="middle">A dragon in a Cornell Box with normal shading.<br>100,012 primitives, 1.1851 seconds.  (CBdragon.dae) </figcaption>
                </tr>
            </table>
        </div>

    <h2 align="middle">Part 3:  Direct Illumination</h2>

        <p>Armed with the our newfound speediness, we are able to tackle global illumination, starting with direct illumination.  

        <h3>Implementation</h3>

        <p>For a given ray which intersects the scene:  we loop over each light, sampling n times (unless the light is a delta-light, for which we only need a single sample) to estimate the direct lighting its intersection.  For each sample, we perform two checks:</p>

        <p>For a given ray which intersects the scene:  we loop over each light (<code>l</code>), sampling n times (unless the light is a delta-light (<code>l->is_delta_light() == true</code>), for which we only need a single sample) to estimate the direct lighting (<code>l->sample_L()</code>) at its intersection.  For each sample, we perform two checks:</p>

            <ul>
                <li>On the returned direction vector (<code>w_in</code>), whether the z-coordinate is negative (<code>w_in.z &lt; 0</code>).  This implies that the light sample originates behind the surface.</li>
                <li>On a shadow ray cast slightly offset from the light's intersection point (<code>hit_p + EPS_D * wi</code>, where <code>wi</code> is <code>w_in</code> in world coordinates), whether the ray intersects with another element of the scene (<code>bvh->intersect(shadowRay)</code>).  This implies occlusion by an object in the scene.</li>
            </ul>

        <p>If either is the case, we know the sampled light does not contribute to the scene's direct lighting, and so can proceed to the next light sample. If not, we take the light sample's radiance, and scale by the following factors before adding it to the running radiance aggregate:</p>

            <ul>
                <li>The cosine of the returned direction vector and the surface normal.  (This is equal to the z component of the returned direction vector (<code>w_in.z</code>) by an enforced local convention of surface normals as the vector (0, 0, 1).)</li>
                <li>The material's BSDF evaluated at the intersection point for the given outgoing and sampled ingoing ray directions (<code>isect.bsdf->f(w_out, w_in)</code>).  This accounts for material properties.</li>
                <li>Division by the pdf (<code>1/pdf</code>).  This counteracts the bias introduced by the pdf used to acquire samples.</li>
            </ul>

        <p> After iterating through all light samples, the resultant aggregate radiance is returned.</p>

        <h3>Render Showcase</h3>

        <p> As demonstrated in the following renders, we are now able to achieve area-light shadows and occlusions:</p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part3_dragon.png" width="480px" />
                    <figcaption align="middle">A dragon sitting on a stone.  Direct illumination.<br>105,120 primitives.  (dragon.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part3_CBbunny.png" width="480px" />
                    <figcaption align="middle">A bunny in a Cornell Box.  Direct illumination.<br>28,588 primitives.  (CBbunny.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part3_CBspheres_lambertian.png" width="480px" />
                    <figcaption align="middle">Two spheres in a Cornell Box.  Direct illumination.<br>14 primitives.  (CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <p> Lastly, let us explore the impact of the number of light samples. With 1, 4, 16, and 64 light samples, respectively, notice the increasing reduction in noise for the soft shadows:  </p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part3_CBspheres_lambertian_1samp.png" width="480px" />
                    <figcaption align="middle">1 sample per area light.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part3_CBspheres_lambertian_4samp.png" width="480px" />
                    <figcaption align="middle">4 samples per area light.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part3_CBspheres_lambertian_16samp.png" width="480px" />
                    <figcaption align="middle">16 samples per area light.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part3_CBspheres_lambertian_64samp.png" width="480px" />
                    <figcaption align="middle">64 samples per area light.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

    <h2 align="middle">Part 4:  Indirect Illumination</h2>

        <p>We next tackle the latter portion of global illumination--indirect illumination.  This will lend our renders more realistic properties, including color-bleeding on lambertian surfaces, as well as light upon surfaces not reachable with out tracing multiple bounces (namely, the ceiling in Cornell Box scenes).</p>

        <h3>Implementation</h3>

        <p>At a glance, for each ray, we sample the BSDF for a spectrum, use this to determine the termination probability for Russian Roulette, and if not terminated recursively trace the ray's next bounce.<p>

        <p>To elaborate:</p>

        <p>Firstly, sampling the BSDF.  Specifically, we sample the BSDF previously stored as intersection information, passing in the outgoing direction and containers for the sampled ingoing direction and evaluated PDF (</code>bsdfSpec = isect.bsdf->sample-f(w_out, &amp;w_in, &amp;pdf)</code>).</p>

        <p>We next perform Russian Roulette to determine whether this ray is to terminate.  (If it does, its contribution is not included in the aggregate radiance.)  Our reason, however, for performing this check after sampling the BSDF is that we base our termination probability on the BSDF's spectrum's illumination (<code>bsdfSpec.illum()</code>), so that rays with less influence are more likely to terminate.  We also include a multiplier (<code>illumMultiplier</code>) so that the influence of the Russian Roulette can modulated.  (Namely, a higher multiplier prolongs termination.)  Thus, our termination probability is the value <code>1 - bsdfSpec.illum() * illumMultiplier</code>once it has been clamped to the interval [0, 1].</p>

        <p>If the ray is not terminated, generate a ray representing it's next bounce--a ray whose origin is slightly offset from the intersection point, and whose direction is the BSDF's returned incoming direction (<code>Ray(hit_p + EPS_D * o2w * w_in, o2w * w_in)</code>).</p>

        <p>Lastly, we make a recursive call to <code>PathTracer::trace_ray()</code>, whose output is scaled by the following factors before being returned:</p>

            <ul>
                <li>The BSDF's spectrum (<code>bsdfSpec</code>).  This accounts for material properties.</li>
                <li>The cosine of the returned direction vector and the surface normal.  (As in direct illumination, this is equal to the z component of the returned incoming direction vector (<code>w_in.z</code>) by an enforced local convention of surface normals as the vector (0, 0, 1).)</li>
                <li>Division by the pdf (<code>1/pdf</code>).  This counteracts the bias introduced by the pdf used to acquire samples.</li>
                <li>Division by the probability of not terminating (<code>1/(1 - terminationProb)</code>).  This counteracts the bias introduced by Russian Roulette.</li>
            </ul>

        <p> After recursively tracing a ray's bounces to termination, the aggregate radiance is returned.<p>

        <h3>Render Showcase</h3>

        <p>As mentioned in the section introduction, notice the red and blue color bleeding onto other elements of the scene such as the center figures and box's grey panels. In addition, the ceiling panel and undersides of figures are more visibly lit.</p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part4_CBbunny.png" width="480px" />
                    <figcaption align="middle">A bunny in a Cornell Box.  Global illumination.<br>28,588 primitives.  (CBbunny.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part4_CBspheres_lambertian.png" width="480px" />
                    <figcaption align="middle">Two spheres in a Cornell Box.  Global illumination.<br>14 primitives.  (CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <p>To dissect the contribution of direct versus indirect lighting, let us observe the spheres scene rendered first with only direct lighting, and then with only indirect lighting.  One will notice that while direct illumination provides first-bounce lighting (ie. portions directly under the area light are lit, whereas those occluded are completely dark), indirect lighting provides the subsequent bounces' lighting contributions which complementarily introduce lighting such as on the undersides of scene elements.</p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part4_CBspheres_lambertian_direct.png" width="480px" />
                    <figcaption align="middle">Two spheres in a Cornell Box.  Direct illumination only.<br>14 primitives.  (CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part4_CBspheres_lambertian_indirect.png" width="480px" />
                    <figcaption align="middle">Two spheres in a Cornell Box.  Indirect illumination only.<br>14 primitives.  (CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <p>Lastly, let us explore the impact of the samples-per-pixel rate on global illumination.  With 1, 16, 256, and 2048 samples per pixel, notice the significant reduction in the noise throughout the scene!  As such, the renders achieve a much smoother, cleaner look.<p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part4_CBspheres_lambertian_1samp.png" width="480px" />
                    <figcaption align="middle">1 sample per pixel.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part4_CBspheres_lambertian_16samp.png" width="480px" />
                    <figcaption align="middle">16 samples per pixel.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part4_CBspheres_lambertian_256samp.png" width="480px" />
                    <figcaption align="middle">256 samples per pixel.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part4_CBspheres_lambertian_2048samp.png" width="480px" />
                    <figcaption align="middle">2048 samples per pixel.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

    <h2 align="middle">Part 5: Materials</h2>
        <p> As a finishing touch on our renderer, we add a few material BSDF's:  mirror surfaces, and glass.</p>

        <h3>Implementation</h3>

        <p>Firstly, the mirror surface.  As we wish to model a perfect mirror, we assume that the surface purely reflects the entirety of incoming light.  Furthermore, as a delta BSDF, we already know that the returned pdf evaluation is equal to 1.  Beyond that, we simply must do the following:</p>

            <ul>
                <li>Create a reflection helper method to determine the incoming light direction (<code>wi</code>) given an outgoing direction (<code>wo</code>) about a normal (<code>n</code>). This can easily be computed as <code>2 * dot(wo, n) * n - wo</code>.</li>
                <li>Return the surface's reflectance, scaled (in this case, divided) by a cosine term (<code>abs_cos_theta(*wi)</code>).  This converts the radiance back to it pure (ie. not modulated by Lambert's law) quantity, as this is already accounted for in our light estimation methods.</li>
            </ul>
        
        <p>And that's all there is to the mirror BSDF!  The glass BSDF, however, is somewhat more complex, as we now introduce refraction.  In constructing a refraction helper method, we proceed as follows:<p>

            <ul>
                <li>Determine whether we are traveling from air to another material, or vice versa.  We can identify this by performing a check on the z-component of the outgoing direction vector.  If negative (<code>wo.z &lt; 0</code>), we are traveling from another medium to air; else, vice-versa.</li>
                <li>Setting our ingoing and outgoing indices of refraction accordingly, we next check whether total internal reflection occurs (<code>ni * sin_theta(wo) >= no</code>), and if so to return early.</li>
                <li>If this is not the case, we determine the spherical coordinates of the ingoing direction vector and convert to cartesian coordinates, storing this before returning <code>true</code> to indicate successful refraction.</li>
            </ul>
    
        <p>Lastly, we put this work in our BSDf implementation:<p>

            <ul>
                <li>Perform a check for total internal reflection, reflecting if this is the case.   (That is, we return a spectrum as we would from the mirror BSDF.)</li>
                <li>Else, we proceed to determine materials' respective indices of refraction (as we did in refract).</li>
                <li>From there, we are equipped to estimate our Fresnel coefficient via Schlick's approximation.  This coefficient, <code>R</code>, can be formulated as follows:

                    <div align="middle"> 
                        <table style="width=100%">
                            <tr>
                                <td align="middle">
                                <img src="images/eqn/part5_R.png" />
                            </tr>
                        </table>
                    </div>                

                where 

                    <div align="middle"> 
                        <table style="width=100%">
                            <tr>
                                <td align="middle">
                                <img src="images/eqn/part5_R0.png" />
                            </tr>
                        </table>
                    </div>   

                and where n<sub>i</sub> and n<sub>o</sub> are our incoming and outgoing materials' indices of refraction, respectively; and &theta;<sub>i</sub> is the angle between the incoming direction and the surface normal.</li>

                <li>We then use this as our probability with which to reflect or refract.  If reflecting, we return as we would from a mirror BSDF, but scaled by <code>R</code>.  Else, we return the following formulation to encapsulate the spectrum of our refracted light:

                    <div align="middle"> 
                        <table style="width=100%">
                            <tr>
                                <td align="middle">
                                <img src="images/eqn/part5_refract.png" />
                            </tr>
                        </table>
                    </div>

                where L<sub>i</sub> is the material's transmittance.    
                </li>

            </ul>

        <h3>Render Showcase</h3>

        <p>Let us take this final opportunity to observe two trends on our complete renders.  Firstly, the impact of maximum ray-depth on rendered specular effects.  One will notice that caustics grow more pronounced, at the the expense of greater overall noise (observe the caustic at the base of the glass sphere, and on the adjacent portion of the blue wall), as we enforce increasingly postponed forced terminations.  This trend continues until we arrive at a "fully converged" image in which Russian Roulette has effectively full control over ray termination.  And so, consider max ray depths of 1, 4, 16, 64, and 100:</p>

  
        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part5_CBspheres_1depth.png" width="480px" />
                    <figcaption align="middle">May ray depth of 1.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                  s  <td align="middle">
                    <img src="images/deliverables/part5_CBspheres_4depth.png" width="480px" />
                    <figcaption align="middle">May ray depth of 4.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part5_CBspheres_16depth.png" width="480px" />
                    <figcaption align="middle">May ray depth of 16.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part5_CBspheres_64depth.png" width="480px" />
                    <figcaption align="middle">May ray depth of 64.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part5_CBspheres_100depth.png" width="480px" />
                    <figcaption align="middle">May ray depth of 100.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <p>Secondly, let us observe the impact of samples-per-pixel rates, at 1, 4, 16, 64, and 1024 samples per pixel.  Similar to the previous section, notice the consistent reduction in noise!</p>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part5_CBspheres_1samp.png" width="480px" />
                    <figcaption align="middle">1 sample per pixel.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part5_CBspheres_4samp.png" width="480px" />
                    <figcaption align="middle">4 samples per pixel.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part5_CBspheres_16samp.png" width="480px" />
                    <figcaption align="middle">16 samples per pixel.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part5_CBspheres_64samp.png" width="480px" />
                    <figcaption align="middle">64 samples per pixel.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/part5_CBspheres_1024samp.png" width="480px" />
                    <figcaption align="middle">1024 samples per pixel.<br>(CBspheres_lambertian.dae) </figcaption>
                </tr>
            </table>
        </div>

        <!-- <p>So there you have it!  As a result of our implementation endeavors, we can now even have our very own Wall-E!</p>

        TODO
        <div align="middle"> 
            <table style="width=100%">
                <tr>
                    <td align="middle">
                    <img src="images/deliverables/so_adorable.png" width="480px" />
                    <figcaption align="middle">It's a Wall-E!<br>(wall-e.dae) </figcaption>
                </tr>
            </table>
        </div> -->

</div>
</body>
</html>
