Welcome, and thank you for taking the time to view my computer graphics portfolio!  See the instructions below:

PathTracer Portfolio: 
- To view, first download the PathTracer folder--this contains the HTML file (pathtracer.html) serving as the portfolio, as well as a directory containing its associated images.
- Then, open pathtracer.html in your browser of choice.

LensSimulator Portfolio:  
- Coming Soon!

